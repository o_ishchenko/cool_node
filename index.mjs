import utils from 'util';
import * as utils2 from 'test-utils';

const params = { password: 'sdf234fdsf32cdsc' };

const coolPromise = utils.promisify(utils2.runMePlease);

try{
    const result = await coolPromise(params);
    console.log(result);
} catch (error){
    console.log(error);
}